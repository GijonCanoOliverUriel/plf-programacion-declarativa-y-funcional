# Mapa conceptual 1
## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001) <<rose>>
	* programación declarativa <<your_style_name>>
		* buscar alternativas expresivas 
			*_ que 
				* nos vengan bien 
					*_ para  
						* dar al ordenador las especificaciones
							* Sobre qué es lo que queremos
		*_ se
			* vasa en el en el modelo de Von neumann
		*_ es
			* un paradigma
				* que surge como reacción a algunos problemas
					* que lleva consigo la programación clásica
			* un estilo de programacion
				*_ donde 
					* básicamente en el que las tareas rutinarias 
						*_ de 
							* programación se dejan al compilador
		*_ ventaja
			* programas más cortos
			* programas más fáciles de realizar
			* programas áciles de depurar
			* se conocen normalmente 
				*_ el 
					* tiempo de desarrollo de una aplicación 
		* tenemos al menos dos variantes principales
			* programacion funcional
				*_ que hace 
					* es recurrir al lenguaje que utilizan las matemáticas
						*_ en 
							* particular al lenguaje que describe funciones
				*_ se 
					* toma la programación 
						*_ como 
							* la aplicación de una función
				*_ características
					* la existencia de funciones de orden superior  
					* tienes como objetos principales tu lenguaje
						*_ las funciones 
							* pueden definir también funciones
							* que actúan a su vez sobre funciones
			* programación lógica
				* en lugar de Recurrir al tipo de lenguaje
					* que utilizan los matemáticos
					*_ la
						* lógica de predicados de primer orden
							*_ pero 
								* es algo que se corresponda 
									* tan 
										* claramente como una función
								* son relaciones entre objetos
									* que estamos definiendo Este tipo \nde relación establecen orden 
										*_ entre 
											* argumentos de entrada 
											* datos de salida
				*_ ventaja
					* nos permite ser más declarativos
						*_ por ejemplo
							* podemos definir una relación factorial entre dos números 
								*_ que 
									* dice si efectivamente uno de los dos números es el factorial
				*_ se 
					* basa en otros modelos
						* el modelo de la demostración de la lógica
						* la demostración automática  
					* se utilizan como expresiones de los programas 
		* conosida tambien 
			*_ como
				* programacion perezosa
				* programación cómoda
				* programación hermosa
		*_ los
			* problemas
				* no se está directamente diciéndole 
					*_ a la 
						* máquina lo que tiene que hacer 
							*_ Entonces 
								* depende muchodel tipo de programas
		*_ consiste
			* en abandonar las secuencias de órdenes 
				*_ que 
					* gestiona la memoria del ordenador
			* tomar una perspectiva más de darle explicaciones 
				*_ de 
					* cómo son los algoritmos 
		*_ propósito
			* Es conseguir reducir la complejidad los programas
				*_ prevenir
					* el riesgo de cometer errores haciendo programas
					* la fragosidad del código una vez que está escrito
	* programación imperativa <<your_style_name>>
		*_ es
			* la programación en
				* java
				* c
				* pascal
	* código máquina <<your_style_name>>
		*_ es
			* el lenguaje propio 
				*_ de 
					* los ordenadores 
					* los programas
			* dar órdenes directas y concretas
				* muy sencillas de muy bajo nivel
			* muy complicado programar de esta forma
				*_ entonces 
					* enseguida se desarrollaron el lenguaje de alto nivel
						*_ como
							* fortran
					* que permitía separarse un poquito 
						*_ de 
							* tan lenguaje tan primitivo
		*_ gestión de la memoria 
			* es relativamente complejas 
				*_ porque 
					* que hay tener mucho cuidado 
						*_ con 
							* cada paso y cada alteración 
@endmindmap
```

# Mapa conceptual 2
## Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Lenguaje de Programación Funcional (2015) <<your_style_name>>
	*_ la
		* programación orientada objetos <<green>>
			*_ son
				* pequeños Trozos de código
					*_ que son 
						* los objetos que van interactuando entré Sí
					*_ que se 
						* componen de instrucciones 
							* que se Ejecutan secuencialmente
	* von Neumann
		*_ que 
			* propuso que los programas <<green>>
				*_ deben 
					* almacenarse en la misma máquina antes de ser ejecutados
				*_ y que 
					* la ejecución consistiría 
						*_ en 
							* una serie de instrucciones 
								*_ que 
									* serían ejecutandas secuencialmente
	*_ podemos 
		* definir una computación de muchas formas diferentes <<green>>
			*_ por ejemplo
				* podemos basarnos en la lógica simbólica
					*_ si
						* nos basamos en la lógica simbólica
							*_ llegamos 
								* al paradigma de programación lógico
				* En el paradigma programación lógico
					*_ un 
						* programa se forma mediante un conjunto de sentencias
							*_ que
								* definen lo que es verdad y lo que es conocido 
								* por un programa con respecto a un problema
	*_ los
		* paradigmas de programación <<green>>
			*_ son
				* básicamente el modelo de computación 
					*_ en 
						* el que los diferentes lenguajes 
							*_ dotan 
								* de semántica a los programas
							*_ estaban 
								* basados en el modelo de computación devon neumann
									*_ que 
										* se llamaba así en honor al matemático von Neumann
	*_ El
		* calculo landan <<green>>
			* inicialmentelo pensaron como sistema 
				*_ para 
					* estudiar el concepto de función 
						*_ de 
							* aplicación de funciones de recursividad
						*_ pero
							* obtuvieron un sistema computacional bastante potente
								*_ Eso significa
									* lo que se puede hace en uno se puede hacer el otro
			* se desarrolló en la década de 1930
				*_ por 
					* Stephen Kleene
				*_ la 
					* lógica combinatoria
						* es una variante
							*_ que 
								* fue los cimientos
									*_ de
										* lo que son las bases de la programación funcional
	*_ un 
		* programa <<green>>
			*_ es 
				* una colección de datos
			*_ una 
				* serie de instrucciones que operan sobre dichos datos
					*_ las 
						* instrucciones se van ejecutando
							*_ en 
								* un orden adecuado
									*_ según
										* el valor de las variables
										* el estado del las variables
	*_ sus
		* características <<green>>
			*_ es que
				*  todo gira alrededor de las funciones
		* ventajas <<green>>
			* no tienes estado de cómputo
			* codigo mas rapido
			* no tiene esas variables 
				*_ que 
					* pueden tener un valor determinado 
						*_ en 
							* cualquier parte del programa
	*_ las
		* variables <<green>>
			* existen pero solamente sirven 
				*_ para 
					* referirse a las parámetros 
						*_ de 
							* entrada de las funciones,
		* constantes <<green>>
			* equiparan a las funciones
				*_ una 
					* función como parámetro de entrada
						*_ se 
							* denomina función de orden superior
								*_ que 
									* se usaba profusamente en programación funcional
				* si una función devuelve siempre el mismo resultado 
					*_ podemos 
						* pesar qué es una función constante
							*_ que 
								* si devuelve el mismo resultado 
								* tiene que ser independientemente 
									*_ de 
										* los parámetros de entrara que reciben
							*_ una 
								* función que no reciba parámetros
									*_ siempre 
										* devuelve el mismo resultado
@endmindmap
```

